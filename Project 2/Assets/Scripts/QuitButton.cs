﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuitButton : MonoBehaviour
{
    public Button quitButton;

    void Start()
    {
        Button btn = quitButton.GetComponent<Button>(); // initializes the component
        btn.onClick.AddListener(TaskOnClick); // allows for the button to be clicked
    }

    void TaskOnClick()
    {
        Application.Quit(); // quits the application
    }
}
