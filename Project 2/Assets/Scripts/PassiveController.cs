﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassiveController : MonoBehaviour
{
    private Rigidbody2D rb;
    private Transform tf;
    public float enemySpeed;
    private Vector3 direction;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>(); // initializes the component
        tf = GetComponent<Transform>(); // initializes the component
        direction = (tf.position - GameManager.instance.playerPosition.position).normalized; // rotates the enemy to the players position
        tf.up = direction; // updates the direction
    }

    void Update()
    {
        rb.velocity = -transform.up * enemySpeed; // moves the enemy
    }
    void OnBecameInvisible()
    {
        GameManager.instance.activeEnemies.Remove(this.gameObject); // removes the enemy from the list
        Destroy(gameObject); // destroys the enemy
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            GameManager.instance.Death(); // destroys the player
        }
    }
}
