﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public bool ending;
    public int lives;
    public int score;
    public Transform playerPosition;
    public Text livesUI;
    public Text scoreUI;
    public Text end;
    public Text restart;
    
    public List<GameObject> activeEnemies = new List<GameObject>();
    

    void Awake() // ensures that there is always a gamemanager and that there is only ever one
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        
    }

    void Update()
    {
        lives = int.Parse(livesUI.text); // updates the lives
        score = int.Parse(scoreUI.text); // updates the score

        if (lives < 0)
        {
            end.text = "You have Achieved Death"; // shows text
            restart.text = "Press R to restart"; // show text
            lives = 0; // makes sure the lives arn't negative
            livesUI.text = lives.ToString(); // updates the lives
            ending = !ending; // toggles ending
            for (int i = 0; i < activeEnemies.Count; i++)
            {
                Destroy(activeEnemies[i]); // destroys all of the enemies
            }
            activeEnemies.Clear(); // clears the enemies from the list
        }

        if (lives == 0)
        {
            if (Input.GetKeyDown("escape"))
            {
                Application.Quit(); // quits the application
            }
        }
    }

    

    public void Death()
    {
        playerPosition.position = Vector3.zero; // resets the players position
        lives -= 1; // decreases the number of lives
        livesUI.text = lives.ToString(); // updates the lives
        for (int i = 0; i < activeEnemies.Count; i++)
        {
            Destroy(activeEnemies[i]); // destroys all of the enemies
        }
        activeEnemies.Clear(); // clears the enemies from the list
    }
}
