﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private Rigidbody2D rb;
    private Transform tf;
    public GameObject shot;
    public float enemySpeed;
    private Vector3 direction;
    public float fireRate;
    private float nextFire;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>(); // initializes the component
        tf = GetComponent<Transform>(); // initializes the component

    }

    void Update()
    {
        rb.velocity = -tf.up * enemySpeed; // makes the enemy move
        direction = (tf.position - GameManager.instance.playerPosition.position).normalized; // rotates the enemy to look at the player
        tf.up = direction; // updates the direction
    }
    void OnBecameInvisible() // when the enemy leaves the screen
    {
        GameManager.instance.activeEnemies.Remove(this.gameObject); // removes it from the list
        Destroy(gameObject); // destroys the object
    }
    void OnTriggerEnter2D(Collider2D other) // checks for a collision
    {
        if (other.CompareTag("Player")) // if the tag is player
        {
            GameManager.instance.Death(); // calls a function from the game manager
        }
    }
}
