﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb;
    public float speed;
    public float rotationSpeed;
    public GameObject shot;
    public Transform tf;
    public float fireRate;
    private float nextFire;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>(); // initializes the component
    }

    void Update()
    {
        if (Input.GetKey("w"))
        {
            rb.velocity = tf.up * speed; // makes the player move forward
        }
        if (Input.GetKey("s"))
        {
            rb.velocity = -tf.up * speed; // makes the player move backward
        }
        if (Input.GetKey("d"))
        {
            tf.Rotate(0.0f, 0.0f, -rotationSpeed); // rotates the player
        }
        if (Input.GetKey("a"))
        {
            tf.Rotate(0.0f, 0.0f, rotationSpeed); // rotates the player
        }

        if (Input.GetKey("space") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate; // sets nextfire to create a delay
            Instantiate(shot, tf.position + (tf.up * 0.7f) + (tf.right * 0.1f), tf.rotation); // spawns a bullet
        }

        if (Input.GetKeyDown("r"))
        {
            SceneManager.LoadScene("Menu"); // switches scenes
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Border")
        {
            GameManager.instance.Death(); // kills the player
        }
    }
}
