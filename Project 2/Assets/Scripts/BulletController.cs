﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    private Rigidbody2D rb;
    public float bulletSpeed;
    public float bulletDecay;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>(); // initializes the component
    }

    void Update()
    {
        rb.velocity = transform.up * bulletSpeed; // moves the bullet

        bulletDecay -= Time.deltaTime; // sets the bullet decay

        if (bulletDecay <= 0.0f) // checks if it should kill the bullet
        {
            Destroy(); // kills the bullet
        }
    }

    void Destroy()
    {
        Destroy(gameObject); // destroys the game object
    }

    void OnTriggerEnter2D(Collider2D other) // checks for collisiions
    {
        if (other.CompareTag("Enemy")) // checks if the tag is Enemy
        {
            GameManager.instance.score += 100; // increases the score
            GameManager.instance.scoreUI.text = GameManager.instance.score.ToString(); // uodates the text
            GameManager.instance.activeEnemies.Remove(other.gameObject); // removes the other object from the list
            Destroy(other.gameObject); // destroys the other object
            Destroy(gameObject); // destroys its self
        }
    }
}
