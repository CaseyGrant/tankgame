﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public List<Transform> spawnPoints;
    public List<GameObject> enemyTypes;
    public int maxEnemies;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.ending) // checks if the game is over
        {
            if (GameManager.instance.activeEnemies.Count < maxEnemies) // checks if the amount of enemies is correct
            {
                SpawnEnemy(); // spawns an enemy
            }
        }
    }
    void SpawnEnemy()
    {
        int type = Random.Range(0, enemyTypes.Count); // gets a random enemy type
        int spawn = Random.Range(0, spawnPoints.Count); // gets a random spawn location
        GameObject enemy = Instantiate(enemyTypes[type], spawnPoints[spawn]); // creates an enemy at a given location
        GameManager.instance.activeEnemies.Add(enemy); // adds the enemy to the list
    }
}
