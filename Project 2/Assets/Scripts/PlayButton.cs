﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayButton : MonoBehaviour
{
    public Button playButton;

    void Start()
    {
        Button btn = playButton.GetComponent<Button>(); // initializes the component
        btn.onClick.AddListener(TaskOnClick); // adds a listener to check for the click
    }

    void TaskOnClick() // runs when clicked
    {
        SceneManager.LoadScene("Tank Game"); // changes scenes
    }
}
